/*******************************************************************************
 * pidialer
 *
 * phonenumber.c:  Handle display of phone number.
 *
 * License: BSD0
 *
 ******************************************************************************
 * Notes:
 * Being a C file (and not Java), please adhere to the 80 character line
 * length standard in this file.
 * 
 * Based on:
 * Custom GTK+ widget - http://zetcode.com/tutorials/gtktutorial/customwidget/
 * Clock Widget - https://github.com/humbhenri/clocks/tree/master/gtk-clock
 ******************************************************************************/
#define PHONENUMBERWIDGET_C

/* To avoid deprecation warnings from GTK+ 2.x */
#pragma GCC diagnostic ignored "-Wdeprecated-declarations"

#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <math.h>
#include <glib.h>
#include <pibox/log.h>
#include "phonenumberwidget.h"

#define FONT_SIZE    80

/* Default base color */
GdkColor    base;

/*
 * Local copy of widget structure so some callbacks can
 * access it.
 */
static GtkPhonenumber *local_phonenumber = NULL;

/*
 *========================================================================
 * Prototypes
 *========================================================================
 */
static void gtk_phonenumber_class_init(GtkPhonenumberClass *klass);
static void gtk_phonenumber_init(GtkPhonenumber *phonenumber);
static void gtk_phonenumber_size_request(GtkWidget *widget, GtkRequisition *requisition);
static void gtk_phonenumber_size_allocate(GtkWidget *widget, GtkAllocation *allocation);
static void gtk_phonenumber_realize(GtkWidget *widget);
static gboolean gtk_phonenumber_expose(GtkWidget *widget, GdkEventExpose *event);
static void gtk_phonenumber_paint(GtkWidget *widget);
static void gtk_phonenumber_destroy(GtkObject *object);

/*
 *========================================================================
 *========================================================================
 * Private API
 *========================================================================
 *========================================================================
 */
GtkType
gtk_phonenumber_get_type(void)
{
    static GtkType gtk_phonenumber_type = 0;
    if (!gtk_phonenumber_type) {
        static const GtkTypeInfo gtk_phonenumber_info = {
            "GtkPhonenumber",
            sizeof(GtkPhonenumber),
            sizeof(GtkPhonenumberClass),
            (GtkClassInitFunc) gtk_phonenumber_class_init,
            (GtkObjectInitFunc) gtk_phonenumber_init,
            NULL,
            NULL,
            (GtkClassInitFunc) NULL
        };
        gtk_phonenumber_type = gtk_type_unique(GTK_TYPE_WIDGET, &gtk_phonenumber_info);
    }
    return gtk_phonenumber_type;
}

GtkWidget * gtk_phonenumber_new()
{
    return GTK_WIDGET(gtk_type_new(gtk_phonenumber_get_type()));
}

static void
gtk_phonenumber_class_init(GtkPhonenumberClass *klass)
{
    GtkWidgetClass *widget_class;
    GtkObjectClass *object_class;

    widget_class = (GtkWidgetClass *) klass;
    object_class = (GtkObjectClass *) klass;

    widget_class->realize = gtk_phonenumber_realize;
    widget_class->size_request = gtk_phonenumber_size_request;
    widget_class->size_allocate = gtk_phonenumber_size_allocate;
    widget_class->expose_event = gtk_phonenumber_expose;

    object_class->destroy = gtk_phonenumber_destroy;
}

static void
gtk_phonenumber_init(GtkPhonenumber *phonenumber)
{
    phonenumber->pixmap = NULL;
    phonenumber->number = NULL;
    base.red   = 200;
    base.green = 200;
    base.blue  = 200;
    local_phonenumber = phonenumber;
}

static void
gtk_phonenumber_size_request(GtkWidget *widget, GtkRequisition *requisition)
{
    g_return_if_fail(widget != NULL);
    g_return_if_fail(GTK_IS_PHONENUMBER(widget));
    g_return_if_fail(requisition != NULL);
}

static void
gtk_phonenumber_size_allocate(GtkWidget *widget, GtkAllocation *allocation)
{
    g_return_if_fail(widget != NULL);
    g_return_if_fail(GTK_IS_PHONENUMBER(widget));
    g_return_if_fail(allocation != NULL);

    widget->allocation = *allocation;
    if (GTK_WIDGET_REALIZED(widget)) {
        gdk_window_move_resize(
            widget->window,
            allocation->x, allocation->y,
            allocation->width, allocation->height
        );
    }
}

static void
gtk_phonenumber_realize(GtkWidget *widget)
{
    GdkWindowAttr attributes;
    guint attributes_mask;
    GtkStyle *style;
    GtkAllocation alloc;

    g_return_if_fail(widget != NULL);
    g_return_if_fail(GTK_IS_PHONENUMBER(widget));

    GTK_WIDGET_SET_FLAGS(widget, GTK_REALIZED);

    gtk_widget_get_allocation(widget, &alloc);

    piboxLogger(LOG_INFO, "alloc.x: %d\n", alloc.x);
    piboxLogger(LOG_INFO, "alloc.y: %d\n", alloc.y);
    piboxLogger(LOG_INFO, "alloc.w: %d\n", alloc.width);
    piboxLogger(LOG_INFO, "alloc.h: %d\n", alloc.height);

    attributes.window_type = GDK_WINDOW_CHILD;
    attributes.x           = alloc.x;
    attributes.y           = alloc.y;
    attributes.width       = alloc.width;
    attributes.height      = alloc.height;
    attributes.wclass      = GDK_INPUT_OUTPUT;
    attributes.event_mask  = gtk_widget_get_events(widget) | GDK_EXPOSURE_MASK;
    attributes_mask        = GDK_WA_X | GDK_WA_Y;

    widget->window = gdk_window_new(
        gtk_widget_get_parent_window (widget),
        & attributes, attributes_mask
    );

    style = gtk_widget_get_style (widget);
    if (style != NULL) 
    {
        GTK_PHONENUMBER(widget)->bg = style->bg[GTK_STATE_NORMAL];
        GTK_PHONENUMBER(widget)->fg = style->fg[GTK_STATE_NORMAL];
    }

    gdk_window_set_user_data(widget->window, widget);
    widget->style = gtk_style_attach(widget->style, widget->window);
    gtk_style_set_background(widget->style, widget->window, GTK_STATE_NORMAL);
}

static gboolean
gtk_phonenumber_expose(GtkWidget *widget, GdkEventExpose *event)
{
    g_return_val_if_fail(widget != NULL, FALSE);
    g_return_val_if_fail(GTK_IS_PHONENUMBER(widget), FALSE);
    g_return_val_if_fail(event != NULL, FALSE);

    // Copy the entire pixmap over the widget's drawing area.
    if ( GTK_PHONENUMBER(widget)->pixmap != NULL )
    {
        gdk_draw_drawable(widget->window,
            widget->style->fg_gc[GTK_WIDGET_STATE(widget)], GTK_PHONENUMBER(widget)->pixmap,
            event->area.x, event->area.y,
            event->area.x, event->area.y,
            event->area.width, event->area.height);
    }
    return TRUE;
}

/*
 *========================================================================
 * Name:    gtk_phonenumber_paint
 * Prototype:   void gtk_phonenumber_paint( GtkWidget *widget )
 *
 * Description:
 * Handle painting (data update) of the widget.
 *========================================================================
 */
static void
gtk_phonenumber_paint(GtkWidget *widget)
{
    int                     width, height;
    cairo_t                 *cr_pixmap;
    cairo_surface_t         *cst;
    cairo_t                 *cr;
    GdkPixmap               *pixmap;
    static int              painting = 0;

    PangoFontDescription    *desc;
    PangoRectangle          pangoRectangle;
    PangoAttrList           *attrs = NULL;
    PangoLayout             *layout;
    char                    buf[256];
    gint                    fontSize;

    /* Avoid multiple calls */
    if ( painting )
        return;

    painting = 1;
    piboxLogger(LOG_TRACE2, "Entered\n");

    if ( GTK_PHONENUMBER(widget)->pixmap != NULL )
        g_object_unref( GTK_PHONENUMBER(widget)->pixmap );
    GTK_PHONENUMBER(widget)->pixmap = 
        gdk_pixmap_new(widget->window,widget->allocation.width,widget->allocation.height,-1);
    pixmap = GTK_PHONENUMBER(widget)->pixmap;

    piboxLogger(LOG_TRACE2, "Getting cr\n");
    gdk_drawable_get_size(pixmap, &width, &height);
    cst = cairo_image_surface_create(CAIRO_FORMAT_ARGB32, width, height);
    cr = cairo_create(cst);

    // Fill with white background with the default color
    cairo_set_source_rgb(cr, 1.0, 1.0, 1.0);
    cairo_rectangle(cr, 0, 0, width, height);
    cairo_fill(cr);

    /*
     * Draw the phone number text.
     */
    fontSize = FONT_SIZE;

    layout = pango_cairo_create_layout(cr);
    sprintf(buf, "Nunito Bold %d", fontSize);
    desc = pango_font_description_from_string( buf );
    pango_font_description_set_absolute_size(desc, fontSize*PANGO_SCALE);
    pango_layout_set_font_description(layout, desc);
    pango_font_description_free(desc);
    pango_layout_set_width(layout, -1); // Disable wrap
    pango_layout_set_spacing(layout, 1);
    pango_layout_set_single_paragraph_mode(layout, FALSE);
    pango_layout_set_alignment(layout, PANGO_ALIGN_LEFT);

    /* Initialize attributes. */
    // cairo_translate(cr, 10, 0);
    attrs = pango_attr_list_new();
    pango_attr_list_insert (attrs, pango_attr_underline_new(PANGO_UNDERLINE_NONE));
    pango_layout_set_attributes (layout, attrs);

    if ( local_phonenumber->number )
    {
        piboxLogger(LOG_INFO, "text: %s\n", local_phonenumber->number);
        pango_layout_set_text(layout, local_phonenumber->number, -1);
    }
    else
    {
        piboxLogger(LOG_INFO, "text: NULL\n");
        pango_layout_set_text(layout, " ", -1);
    }
    pango_layout_get_pixel_size(layout, &pangoRectangle.width, &pangoRectangle.height );
    piboxLogger(LOG_TRACE4, "text w/h: %d / %d\n", pangoRectangle.width, pangoRectangle.height);
    cairo_set_source_rgb(cr, 0, 0, 0);
    pango_cairo_update_layout(cr, layout);

    /* Don't need to center horizontally because pango_layout_set_alignment does that for us. */
    // cairo_translate(cr, 0, pixmap_height-(pangoRectangle.height)-5);
    pango_cairo_show_layout(cr, layout);

    /* Cleanup pango */
    pango_attr_list_unref(attrs);
    g_object_unref(layout);

    piboxLogger(LOG_INFO, "Paint the pixmap.\n");
    cr_pixmap = gdk_cairo_create(pixmap);
    cairo_set_source_surface (cr_pixmap, cst, 0, 0);
    cairo_paint(cr_pixmap);
    cairo_destroy(cr_pixmap);

    /* Don't need the cairo object now */
    cairo_surface_destroy(cst);
    cairo_destroy(cr);

    piboxLogger(LOG_INFO, "Done - Paint the pixmap.\n");
    painting = 0;
}

static void
gtk_phonenumber_destroy(GtkObject *object)
{
    // GtkPhonenumber *phonenumber;
    GtkPhonenumberClass *klass;

    g_return_if_fail(object != NULL);
    g_return_if_fail(GTK_IS_PHONENUMBER(object));

    // phonenumber = GTK_PHONENUMBER(object);
    klass = gtk_type_class(gtk_widget_get_type());
    if (GTK_OBJECT_CLASS(klass)->destroy) {
        (* GTK_OBJECT_CLASS(klass)->destroy) (object);
    }
}

/*
 *========================================================================
 *========================================================================
 * Public API
 *========================================================================
 *========================================================================
 */

void
gtk_phonenumber_set( GtkPhonenumber *phonenumber, char *number )
{
    if ( number == NULL )
        return;

    /*
     * Retrieve data for use in updating the scan widget.
     */
    piboxLogger(LOG_INFO, "New number: %s\n", number);
    if ( phonenumber->number )
        free(phonenumber->number);
    phonenumber->number = strdup(number);
    gtk_phonenumber_paint(GTK_WIDGET(phonenumber));
}

char *
gtk_phonenumber_get( GtkPhonenumber *phonenumber )
{
    if ( phonenumber->number )
        return strdup(phonenumber->number);
    else
        return NULL;
}
