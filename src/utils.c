/*******************************************************************************
 * PiDialer
 *
 * utils.c:  Various utility methods
 *
 * License: see LICENSE file
 ******************************************************************************
 * Notes:
 * Being a C file (and not Java), please adhere to the 80 character line
 * length standard in this file.
 ******************************************************************************/
#define UTILS_C

/* To avoid deprecation warnings from GTK+ 2.x */
#pragma GCC diagnostic ignored "-Wdeprecated-declarations"

#include <stdio.h>
#include <ctype.h>

#include "pidialer.h"

/*
 *========================================================================
 * Name:   trimReverse
 * Prototype:  char *trimReverse( char * )
 *
 * Description:
 * Trim whitespace from the end of a buffer by replacing spaces with \0.
 *
 * Notes:
 * Assumes null terminated string as function argument.
 *========================================================================
 */
void
trimReverse( char *ptr )
{
    // Trim leading space
    while(isspace(*ptr)) 
	{
		*ptr = '\0';
        ptr--;
	}
}
