/*******************************************************************************
 * PiDialer
 *
 * pidialer.c:  program main
 *
 * License: BSD0
 *
 ******************************************************************************
 * Notes:
 * Being a C file (and not Java), please adhere to the 80 character line
 * length standard in this file.
 ******************************************************************************/
#define PIDIALER_C

/* To avoid deprecation warnings from GTK+ 2.x */
#pragma GCC diagnostic ignored "-Wdeprecated-declarations"

#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <limits.h>
#include <cairo.h>
#include <gtk/gtk.h>
#include <gdk/gdkkeysyms.h>
#include <pibox/pibox.h>
#include <pigtk/pigtk-keyboard.h>
#include <pigtk/pigtk-textline.h>

#include "pidialer.h" 

/* Local prototypes */
static gboolean draw_phonenumber(GtkWidget *widget, GdkEvent *event, gpointer user_data);
gboolean iconTouchGTK( gpointer data );
void makeCall( void );

cairo_t *cr = NULL;

GtkWidget *darea[16];
GtkWidget *iconTable = NULL;
guint cellWidth = 0;
guint cellHeight = 0;
guint iconPad = 18;
// gint largeFont  = 85;
gint largeFont  = 125;

int currentIdx = 0;

guint homeKey = -1;
int enableTouch = 1;

GdkRectangle displaySize = {0};

#define CALL_IDX        12
#define EXIT_IDX        13
#define ERASE_IDX       14
int keys[15] = {
     1,  2,  3,
     4,  5,  6,
     7,  8,  9,
    -1,  0, -2,
    -3, -3, -3
};
char ckeys[3] = "#* ";

GtkWidget   *phoneNumberEntry;
GtkWidget   *keyboard;

/*
 *========================================================================
 * Name:   loadKeysyms
 * Prototype:  void loadKeysyms( void )
 *
 * Description:
 * Read in the keysym file so we know how the platform wants us to behave.
 *
 * Notes:
 * Format is KEYSYM NAME:ACTION
 *========================================================================
 */
void
loadKeysyms( void )
{
    char        *tsave = NULL;
    struct stat stat_buf;
    char        *keysym;
    char        *action;
    char        *ptr;
    FILE        *fd;
    char        buf[128];
    char        *path;

    if ( isCLIFlagSet( CLI_TEST) )
        path = KEYSYMS_FD;
    else
        path = KEYSYMS_F;

    /* Read in /etc/pibox-keysysm */
    if ( stat(path, &stat_buf) != 0 )
    {
        piboxLogger(LOG_INFO, "No keysym file: %s\n", path);
        return;
    }

    fd = fopen(path, "r");
    if ( fd == NULL )
    {
        piboxLogger(LOG_ERROR, "Failed to open keysyms file: %s - %s\n", path, strerror(errno));
        return;
    }

    memset(buf, 0, 128);
    while( fgets(buf, 127, fd) != NULL )
    {
        /* Ignore comments */
        if ( buf[0] == '#' )
            continue;

        /* Strip leading white space */
        ptr = buf;
        ptr = piboxTrim(ptr);

        /* Ignore blank lines */
        if ( strlen(ptr) == 0 )
            continue;

        /* Strip newline */
        piboxStripNewline(ptr);

        /* Grab first token */
        keysym = strtok_r(ptr, ":", &tsave);
        if ( keysym == NULL )
            continue;

        /* Grab second token */
        action = strtok_r(NULL, ":", &tsave);
        if ( action == NULL )
            continue;

        piboxLogger(LOG_INFO, "keysym / action: %s / %s \n", keysym, action);

        /* Set the home key */
        if ( strncasecmp("home", action, 4) == 0 )
        {
            homeKey = gdk_keyval_from_name(keysym);
            piboxLogger(LOG_INFO, "homeKey = %08x\n", homeKey);
        }
    }
}

/*
 *========================================================================
 * Name:   getDisplaySize
 * Prototype:  void getDisplaySize( void )
 *
 * Description:
 * Sets the display size to use.
 *========================================================================
 */
void
getDisplaySize()
{
#define DEFW      720
#define DEFH      1440

    GdkScreen       *screen;

    displaySize.width  = piboxGetDisplayWidth();
    displaySize.height = piboxGetDisplayHeight();

    if ( (cliOptions.geometryW > 0) || (cliOptions.geometryH > 0) )
    {
        displaySize.width  = cliOptions.geometryW;
        displaySize.height = cliOptions.geometryH;
    }
    else if ( (displaySize.width == 0) || (displaySize.height == 0) )
    {
        screen = gdk_screen_get_default();
        displaySize.width  = (gdk_screen_get_width( screen ) > DEFW)?DEFW:gdk_screen_get_width( screen );
        displaySize.height = (gdk_screen_get_height( screen ) > DEFH)?DEFH:gdk_screen_get_height( screen );
    }

}

/*
 *========================================================================
 * Name:   quitProgram
 * Prototype:  void quitProgram( void )
 *
 * Description:
 * Wrapper to gtk_main_quit() that handles call teardown, etc.
 *========================================================================
 */
void
quitProgram()
{
    // call_end( );
    gtk_main_quit();
}

static gboolean
quitProgramEvent(GtkWidget *widget, GdkEventExpose *event, gpointer user_data)
{
    quitProgram();
    return(FALSE);
}

/*
 *========================================================================
 * Name:   iconTouch
 * Prototype:  void iconTouch( int region )
 *
 * Description:
 * Handler for touch location reports. 
 *========================================================================
 */
void
iconTouch( REGION_T *region )
{
    g_idle_add( (GSourceFunc)iconTouchGTK, (gpointer)region );
}

gboolean
iconTouchGTK( gpointer data )
{
    static int          inprogress = 0;
    int                 idx = 0;
    gint                wx=0, wy=0;
    gint                posX, posY;
    REGION_T            *region = (REGION_T *)data;
    GtkRequisition      req;

    piboxLogger(LOG_INFO, "Entered.\n");

    if ( !enableTouch )
    {
        piboxLogger(LOG_INFO, "Touch processing disabled, skipping update\n");
        return FALSE;
    }

    /* Prevent multiple touches from being handled at the same time. */
    if ( inprogress )
    {
        piboxLogger(LOG_INFO, "In progress, skipping update\n");
        return FALSE;
    }
    inprogress = 1;
    piboxLogger(LOG_INFO, "Processing touch request.\n");

    /* Find the widget that maps to the absolute coordinates */
    while( darea[idx] != NULL )
    {
        /* Get the absolute screen coordinates of the widget */
        gdk_window_get_origin (gtk_widget_get_window (darea[idx]), &wx, &wy);
        req.width = gdk_window_get_width(darea[idx]->window);
        req.height = gdk_window_get_height(darea[idx]->window);
        piboxLogger(LOG_INFO, "origin, wxh: %d %d  %d / %d\n", wx, wy, req.width, req.height);

        /*
         * Set up depending on if we are using rotation or not.
         * We only support CCW rotation currently.
         */
        if ( isCLIFlagSet( CLI_ROTATE) )
        {
            piboxLogger(LOG_INFO, "Rotation is enabled.\n");
            posY = region->x;
            posX = displaySize.height - region->y;
        }
        else
        {
            posX = region->x;
            posY = region->y;
        }
        piboxLogger(LOG_INFO, "touch point, x=%d y=%d\n", posX, posY);

        /* Test if the event was within the bounds of the widget area. */
        if ( (posX >= wx) && (posX<= (wx+req.width)) )
        {
            if ( (posY >= wy) && (posY <= (wy+req.height)) )
            {
                /* Found widget! */
                piboxLogger(LOG_INFO, "Identified table cell: %d\n", idx);
                currentIdx = idx;
                // This needs to go to button_press.
                // appendNum();
                inprogress = 0;
                return(FALSE);
            }
        }
        idx++;
    }
    inprogress = 0;
    piboxLogger(LOG_INFO, "No matching table cell identified for %d / %d\n", region->x, region->y);
    return(FALSE);
}

/*
 *========================================================================
 * Name:   loadDisplayConfig
 * Prototype:  void loadDisplayConfig( void )
 *
 * Description:
 * Read in the display config file so we know how the display should be 
 * handled.
 *
 * Notes:
 * Format is 
 *     DISPLAY TYPE (DVT LCD)
 *     RESOLUTION
 *========================================================================
 */
void
loadDisplayConfig( void )
{
    piboxLoadDisplayConfig();

    /* Are we on a touch screen */
    if ( piboxGetDisplayTouch() )
    {
        setCLIFlag(CLI_TOUCH);
        iconPad = 9;
    }
    else
    {
        unsetCLIFlag(CLI_TOUCH);
    }
}

/*
 *========================================================================
 * Name:   draw_phonenumber
 * Prototype:  void draw_phonenumber( GtkWidget *, GdkEvent *, gpointer )
 *
 * Description:
 * Updates the poster area of the display when realize and expose events occur.
 *========================================================================
 */
static gboolean 
draw_phonenumber(GtkWidget *widget, GdkEvent *event, gpointer user_data)
{      
    char *data;

    data = (char *)calloc(1,2);
    sprintf(data, " ");
    piboxLogger(LOG_INFO, "Initializing gtk_textline.\n");
    gtk_textline_set( phoneNumberEntry, data );
    gtk_widget_queue_draw(GTK_WIDGET(phoneNumberEntry));
    free(data);
    return(FALSE);
}

/*
 *========================================================================
 * Name:   makeCall
 * Prototype:  void makeCall( gint idx )
 *
 * Description:
 * Initiates or hanges up the call.
 *========================================================================
 */
void 
makeCall( )
{
    piboxLogger(LOG_INFO, "Entered makeCall\n");
}

/*
 *========================================================================
 * Name:   exitProgram
 * Prototype:  gboolean exitProgram( gpointer )
 *
 * Description:
 * Scheduled exit.
 *========================================================================
 */
gboolean exitProgram (gpointer data)
{
    quitProgram();
    return TRUE;
}

/*
 *========================================================================
 * Name:   button_press
 * Prototype:  void button_press( GtkWidget *, GdkEventExpose *, gpointer )
 *
 * Description:
 * Catches a button press (as with a mouse).
 *========================================================================
 */
static gboolean
button_press(GtkWidget *widget, GdkEventButton *event, gpointer user_data)
{
    gdouble     x, y;
    char        *key;
    char        *buf = NULL;
    char        *entry;
    int         len;

    entry = gtk_textline_get(phoneNumberEntry);
    x = event->x;
    y = event->y;
    piboxLogger(LOG_INFO, "Position: %f x %f\n", x, y);
    gtk_keyboard_press(keyboard, x, y, GPOINTER_TO_INT(user_data));
    gtk_widget_queue_draw(widget);
    key = gtk_keyboard_get_key(keyboard);
    if ( key )
    {
        piboxLogger(LOG_INFO, "Key: %s\n", key);

        // Was Exit key hit?
        if ( strcasecmp(key, "exit") == 0 )
        {
            // Schedule an exit from the program.
            g_timeout_add(1000, (GSourceFunc)exitProgram, 0);
        }

        // Was Call key hit?
        else if ( strcasecmp(key, "call") == 0 )
        {
            if ( call_state() )
                call_end();
            else
            {
                if ( entry != NULL )
                {
                    // Dial the current number.
                    call_start( entry );
                }
            }
        }

        // Was Erase key hit?
        else if ( strcasecmp(key, "erase") == 0 )
        {
            // Remove a character from the text.
            len = strlen(entry);
            if ( len > 0 )
            {
                entry[len-1] = '\0';
                piboxLogger(LOG_INFO, "Calling gtk_textline_set(%s)\n", entry);
                gtk_textline_set( phoneNumberEntry, entry );
                // gtk_widget_queue_draw(GTK_WIDGET(phoneNumberEntry));
            }
        }

        // Was Erase key hit?
        else if ( entry )
        {
            buf = (char *)calloc(1, strlen(entry) + strlen(key) + 1);
            sprintf(buf, "%s%s", entry, key);
        }

        // Is the entry non-empty?
        else if ( entry )
        {
            buf = (char *)calloc(1, strlen(key) + 1);
            sprintf(buf, "%s", key);
        }

        // The entry is NULL and the key is not special, so just add it.
        else 
        {
            buf = (char *)calloc(1, 2);
            sprintf(buf, "%s", key);
        }

        free(key);
        if ( buf ) 
        {
            gtk_textline_set(phoneNumberEntry, buf);
            free(buf);
        }
    }
    else
        piboxLogger(LOG_INFO, "Key: Unknown\n");

    // Cleanup.
    if ( entry ) 
        free(entry);

    return(TRUE);
}

/*
 *========================================================================
 * Name:   button_release
 * Prototype:  void button_release( GtkWidget *, GdkEventExpose *, gpointer )
 *
 * Description:
 * Catches a button release (as with a mouse).
 *========================================================================
 */
static gboolean
button_release(GtkWidget *widget, GdkEventButton *event, gpointer user_data)
{
    gdouble     x, y;
    x = event->x;
    y = event->y;
    piboxLogger(LOG_INFO, "Position: %f x %f\n", x, y);
    gtk_keyboard_press(keyboard, x, y, GPOINTER_TO_INT(user_data));
    return(TRUE);
}

/*
 *========================================================================
 * Name:   createWindow
 * Prototype:  GtkWidget *createWindow( void )
 *
 * Description:
 * Creates the main window with a scrolled list of icons on the left and currently
 * selected app information on the right.  
 *
 * Notes:
 * If compiled with -DXEON then we don't use the splash screen to save 
 * screen space.
 *========================================================================
 */
GtkWidget *
createWindow()
{
    GtkWidget           *window;
    GtkWidget           *vbox1;

    /* Needed for the text display of the number */
    // PangoFontDescription *fd = NULL;

    window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
    GTK_WIDGET_SET_FLAGS(window, GTK_CAN_FOCUS );

    /* 
     * We need a textline widget with a keyboard underneath.
     */
    vbox1 = gtk_vbox_new (FALSE, 0);
    gtk_widget_set_name (vbox1, "vbox1");
    gtk_container_add (GTK_CONTAINER (window), vbox1);

    /*
     * The text display for the phone number.
     */
    phoneNumberEntry = gtk_textline_new();
    gtk_textline_set_maxlength(phoneNumberEntry, 16);
    gtk_widget_set_size_request(GTK_WIDGET(phoneNumberEntry), displaySize.width, 100);
    gtk_box_pack_start (GTK_BOX (vbox1), phoneNumberEntry, FALSE, TRUE, 0);
    gtk_textline_set_fontsize(phoneNumberEntry, 35);
    g_signal_connect(G_OBJECT(phoneNumberEntry), "map", G_CALLBACK(draw_phonenumber), GINT_TO_POINTER(0)); 

    keyboard = gtk_keyboard_new("pidialer-numentry.json");
    gtk_keyboard_set_fontsize(keyboard, 70);
    gtk_widget_add_events(keyboard, GDK_BUTTON_PRESS_MASK | GDK_BUTTON_RELEASE_MASK);
    gtk_box_pack_start (GTK_BOX (vbox1), keyboard, TRUE, TRUE, 0);
    g_signal_connect(G_OBJECT(keyboard), "button_press_event", G_CALLBACK(button_press), GINT_TO_POINTER(1));
    g_signal_connect(G_OBJECT(keyboard), "button_release_event", G_CALLBACK(button_release), GINT_TO_POINTER(0));

    /* Make the main window die when destroy is issued. */
    g_signal_connect(window, "destroy", G_CALLBACK (quitProgramEvent), NULL);

    /* Now position the window and set its title */
    gtk_window_set_position(GTK_WINDOW(window), GTK_WIN_POS_CENTER);
    gtk_window_set_default_size(GTK_WINDOW(window), displaySize.width, displaySize.height);
    gtk_window_set_title(GTK_WINDOW(window), "pidialer");

    return window;
}

/*
 * ========================================================================
 * Name:   sigHandler
 * Prototype:  static void sigHandler( int sig, siginfo_t *si, void *uc )
 *
 * Description:
 * Handles program signals.
 *
 * Input Arguments:
 * int       sig    Signal that cause callback to be called
 * siginfo_t *si    Structure that includes timer over run information and timer ID
 * void      *uc    Unused but required with sigaction handlers.
 * ========================================================================
 */
static void
sigHandler( int sig, siginfo_t *si, void *uc )
{
    switch (sig)
    {
        case SIGHUP:
            /* Reset the application. */
            quitProgram();
            break;

        case SIGTERM:
            /* Bring it down gracefully. */
            quitProgram();
            break;

        case SIGINT:
            /* Bring it down gracefully. */
            quitProgram();
            break;
    }
}

/*
 * ========================================================================
 * Name:   main
 *
 * Description:
 * Program startup
 * ========================================================================
 */
int 
main(int argc, char *argv[])
{
    GtkWidget *window;
    struct sigaction    sa;

    sa.sa_flags = SA_SIGINFO;
    sa.sa_sigaction = sigHandler;
    sigemptyset(&sa.sa_mask);
    if (sigaction(SIGHUP, &sa, NULL) == -1)
    {   
        fprintf(stderr, "%s: Failed to setup signal handling for SIGHUP.\n", PROG);
        exit(1);
    }
    sa.sa_flags = SA_SIGINFO;
    sa.sa_sigaction = sigHandler;
    sigemptyset(&sa.sa_mask);
    if (sigaction(SIGTERM, &sa, NULL) == -1)
    {
        fprintf(stderr, "%s: Failed to setup signal handling for SIGTERM.\n", PROG);
        exit(1);
    }
    sa.sa_flags = SA_SIGINFO;
    sa.sa_sigaction = sigHandler;
    sigemptyset(&sa.sa_mask);
    if (sigaction(SIGUSR1, &sa, NULL) == -1)
    {
        fprintf(stderr, "%s: Failed to setup signal handling for SIGUSR1.\n", PROG);
        exit(1);
    }
    sa.sa_flags = SA_SIGINFO;
    sa.sa_sigaction = sigHandler;
    sigemptyset(&sa.sa_mask);
    if (sigaction(SIGINT, &sa, NULL) == -1)
    {
        fprintf(stderr, "%s: Failed to setup signal handling for SIGINT.\n", PROG);
        exit(1);
    }

    // Load saved configuration and parse command line
    initConfig();
    parseArgs(argc, argv);

    // Setup logging
    piboxLoggerInit(cliOptions.logFile);
    piboxLoggerVerbosity(cliOptions.verbose);

    validateConfig();

    /* Read environment config for keyboard behaviour */
    loadKeysyms();
    
    /* Dump some configuration status */
    piboxLogger(LOG_INFO, "Verbosity level: %d\n", cliOptions.verbose);
    if ( cliOptions.flags & CLI_LOGTOFILE )
        piboxLogger(LOG_INFO, "Logfile: %s\n", cliOptions.logFile);
    if ( cliOptions.flags & CLI_ROTATE )
        piboxLogger(LOG_INFO, "Rotation enabled.\n");

    /* Get display config information */
    loadDisplayConfig();

    /* 
     * If we're on a touchscreen, register the input handler.
     */
    if ( isCLIFlagSet( CLI_TOUCH ) )
    {
        piboxLogger(LOG_INFO, "Registering imageTouch.\n");
        piboxTouchRegisterCB(iconTouch, TOUCH_ABS);
        piboxTouchStartProcessor();
    }
    else
        piboxLogger(LOG_INFO, "Not a touch screen device.\n");

    // Initialize gtk, create its windows and display them.
    gtk_init(&argc, &argv);
    if ( isCLIFlagSet( CLI_TEST ) )
        gtk_rc_parse("data/gtkrc");

    getDisplaySize();
    piboxLogger(LOG_INFO, "display type: %s\n", 
            (piboxGetDisplayType()==PIBOX_LCD)?"LCD":
            (piboxGetDisplayType()==PIBOX_HDMI)?"HDMI":"Unknown");
    piboxLogger(LOG_INFO, "display resolution: %dx%d\n", 
            displaySize.width, displaySize.height);

    window = createWindow();
    gtk_widget_show_all(window);
    // gtk_widget_queue_draw(GTK_WIDGET(keyboard));
    gtk_keyboard_press(keyboard, -1, -1, 0);

    gtk_main();

    if ( isCLIFlagSet( CLI_TOUCH ) )
    {
        /* Touch processor blocks in a read, so we need to SIGINT to break out of it. */
        raise(SIGINT);
        piboxTouchShutdownProcessor();
    }

    piboxLoggerShutdown();
    return 0;
}
