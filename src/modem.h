/*******************************************************************************
 * PiDialer
 *
 * modem.c:  Handle modem interactions.
 *
 * License: see LICENSE file
 ******************************************************************************
 * Notes:
 * Being a C file (and not Java), please adhere to the 80 character line
 * length standard in this file.
 ******************************************************************************/
#ifndef MODEM_H
#define MODEM_H

/*========================================================================
 * Defined values
 *=======================================================================*/

/*========================================================================
 * TYPEDEFS
 *=======================================================================*/


/*========================================================================
 * Prototypes
 *=======================================================================*/
#ifndef MODEM_C
extern int send_sms(char *phone, char *msg);
extern int call_start(char *phone);
extern int call_end();
extern int call_state();
#endif /* !MODEM_C */
#endif /* !MODEM_H */
