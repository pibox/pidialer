/*******************************************************************************
 * pidialer
 *
 * phonenumberwidget.h:  custom widget for displaying the phone number.
 *
 * License: BSD0
 *
 ******************************************************************************
 * Notes:
 * Being a C file (and not Java), please adhere to the 80 character line
 * length standard in this file.
 ******************************************************************************/
#ifndef __PHONENUMBERWIDGET_H
#define __PHONENUMBERWIDGET_H

#include <gtk/gtk.h>
#include <cairo.h>

G_BEGIN_DECLS

#define GTK_PHONENUMBER(obj) GTK_CHECK_CAST(obj, gtk_phonenumber_get_type (), GtkPhonenumber)
#define GTK_PHONENUMBER_CLASS(klass) GTK_CHECK_CLASS_CAST(klass, gtk_phonenumber_get_type(), GtkPhonenumberClass)
#define GTK_IS_PHONENUMBER(obj) GTK_CHECK_TYPE(obj, gtk_phonenumber_get_type())

typedef struct _GtkPhonenumber GtkPhonenumber;
typedef struct _GtkPhonenumberClass GtkPhonenumberClass;

struct _GtkPhonenumber {
  GtkDrawingArea parent;
  GdkPixmap *pixmap;
  GdkColor bg;
  GdkColor fg;
  char *number;
};

struct _GtkPhonenumberClass {
  GtkDrawingAreaClass parent_class;
};

/* Prototypes */
GtkType gtk_phonenumber_get_type(void);
GtkWidget *gtk_phonenumber_new();
void gtk_phonenumber_set( GtkPhonenumber *phonenumber, char *number );
char * gtk_phonenumber_get( GtkPhonenumber *phonenumber );

G_END_DECLS

#endif /* __PHONENUMBERWIDGET_H */
