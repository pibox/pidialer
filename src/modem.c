/*******************************************************************************
 * PiDialer
 *
 * modem.c:  Handle modem interactions.
 *
 * License: BSD0
 *
 ******************************************************************************
 * Notes:
 * Being a C file (and not Java), please adhere to the 80 character line
 * length standard in this file.
 ******************************************************************************/
#define MODEM_C

/* To avoid deprecation warnings from GTK+ 2.x */
#pragma GCC diagnostic ignored "-Wdeprecated-declarations"

#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <fcntl.h>
#include <termios.h>
#include <termios.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <pibox/pibox.h>
#include <pthread.h>

#include "pidialer.h" 

#define MODEM_PORT          "/dev/ttyAMA0"

/* Modem file descriptor */
static int fd = -1;

/* Current call state */
static int current_state = 0;

/* Mutex for setting/reading call state. */
static pthread_mutex_t callStateMutex = PTHREAD_MUTEX_INITIALIZER;

/*
 *========================================================================
 *========================================================================
 *
 * Static functions
 *
 *========================================================================
 *========================================================================
 */

/*
 *========================================================================
 * Name:   open_modem
 * Prototype:  int open_modem( void )
 *
 * Description:
 * Opens the modem port.
 *
 * Returns:
 * The file descriptor referencing the modem or -1 if the modem port can't
 * be opened.  The file descriptor is also kept in a module static variable.
 *========================================================================
 */
static int 
open_modem()
{
    struct stat stat_buf;

    if ( stat(MODEM_PORT, &stat_buf) != 0 )
    {
        piboxLogger(LOG_ERROR, "Can't find modem port: %s, reason=%s\n", MODEM_PORT, strerror(errno));
        return(-1);
    }

    fd = open(MODEM_PORT, O_RDWR | O_NOCTTY | O_NDELAY);
    if(fd < 0)
    {
        piboxLogger(LOG_ERROR, "Could not open the serial device");
        return(-1);
    }
    return(fd);
}

/*
 *========================================================================
 * Name:   serial_init
 * Prototype:  int serial_init( int fd )
 *
 * Description:
 * Initializes the serial port connected to the modem.
 *
 * Returns:
 * 0 on success or -1 on failure.
 *========================================================================
 */
static int
serial_init(int fd)
{
    struct termios  termios;
    int             ret = 0;

    ret = tcgetattr (fd, &termios);
    if(ret < 0)
    {
        piboxLogger(LOG_ERROR, "tcgetattr failed: %s\n", strerror(errno));
        return(-1);
    }

    /* 115200 Baud */
    cfsetispeed(&termios, B115200);
    cfsetospeed(&termios, B115200);

    /* Disable flow control, canonical mode and character echo. */
    termios.c_iflag &= ~(IXANY | IXON | IXOFF);
    termios.c_lflag &= ~(ICANON | ECHO);

    /* Allow reading but ignore carrier detect line (assert it's always enabled). */
    termios.c_cflag |= (CLOCAL | CREAD);

    /* Set min characters to receive and a timeout to read them. */
    termios.c_cc[VMIN]  = 1;
    termios.c_cc[VTIME] = 10;

    /* Now change these settings on the modem port. */
    ret = tcsetattr (fd, TCSANOW, &termios);
    if(ret < 0)
    {
        piboxLogger(LOG_ERROR, "tcsetattr failed: %s\n", strerror(errno));
        return(-1);
    }

    return(0);
}

/*
 *========================================================================
 * Name:   write_to_port
 * Prototype:  int write_to_port( int fd, char *cmd )
 *
 * Description:
 * Writes the specified command to the modem port.  
 *
 * Returns:
 * 0 on success or <0 on failure.
 *========================================================================
 */
static int
write_to_port(int fd, char *cmd)
{
    piboxLogger(LOG_INFO, "Writing to Port\t: %s\n", cmd);

    int ret = 0;
    ret = write(fd, cmd, strlen(cmd));
    if(ret <= 0)
    {
        piboxLogger(LOG_ERROR, "write failed: %s\n", strerror(errno));
    }
    return ret;
}

/*
 *========================================================================
 * Name:   read_from_port
 * Prototype:  int read_from_port( int fd, char *msg )
 *
 * Description:
 * Reads a response from the modem and places it in msg which must be large
 * enough to hold the response.
 *
 * Returns:
 * 0 on success or <0 on failure.
 *========================================================================
 */
static int
read_port(int fd, char *msg, int len)
{
    int rc = 0;
    rc = read(fd, msg, len);
    return rc;
}

/*
 *========================================================================
 * Name:   init_modem
 * Prototype:  int init_modem( void )
 *
 * Description:
 * Initializes the modem to prepare to make a call or send an SMS.
 *
 * Returns:
 * 0 on success or <0 on failure.
 *========================================================================
 */
static int
init_modem()
{
    char    msg[64];
    int     lfd;

    if ( fd > 0 )
    {
        piboxLogger(LOG_ERROR, "Can't initialize modem: modem is in use.\n");
        return(-1);
    }

    /* Prepare modem. */
    lfd = open_modem();
    if ( lfd < 0 )
        return(-1);
    if ( serial_init(lfd) < 0 )
    {
        close(lfd);
        return(-1);
    }

    if ( ! write_to_port(lfd, "AT\r") )       {close(lfd); return(-1); }
    read_port(lfd, msg, sizeof(msg));

    if ( ! write_to_port(lfd, "ATE0\r") )     {close(lfd); return(-1); }
    read_port(lfd, msg, sizeof(msg));

    if ( ! write_to_port(lfd, "AT\r") )       {close(lfd); return(-1); }

    if ( ! read_port(lfd, msg, sizeof(msg)) ) {close(lfd); return(-1); }

    piboxLogger(LOG_INFO, "Modem initialization respone: %s\n", msg);
    return(0);
}

/*
 *========================================================================
 *========================================================================
 *
 * Global functions
 *
 *========================================================================
 *========================================================================
 */

/*
 *========================================================================
 * Name:   send_sms
 * Prototype:  int send_sms( char *phoneNumber, char *msg )
 *
 * Description:
 * Initializes the modem to prepare to make a call or send an SMS.
 *
 * Inputs:
 * char *phoneNumber        Number to send the text to.
 * char *textMsg            The text to send.
 *
 * Returns:
 * 0 on success or <0 on failure.
 *
 * Notes:
 * "fd" is a module static variable set by init_modem().
 *========================================================================
 */
int
send_sms(char *phoneNumber, char *textMsg)
{
    char    buf[32];
    char    msg[64];

    if ( init_modem() < 0 )
        return(-1);

    /* Get modem's attention. */
    if ( ! write_to_port(fd, "AT\r") )
    {
        close(fd);
        return(-1);
    }
    read_port(fd, msg, sizeof(msg));
    piboxLogger(LOG_TRACE1, "AT response: %s\n", msg);
    memset(msg, 0, 64);

    /* TBD */
    if ( ! write_to_port(fd, "AT+CMGF=1\r\n") )
    {
        close(fd);
        return(-1);
    }
    read_port(fd, msg, sizeof(msg));
    piboxLogger(LOG_TRACE1, "AT response: %s\n", msg);
    memset(msg, 0, 64);

    /* TBD */
    if ( ! write_to_port(fd, "AT+CNMI=2,1,0,0,0\r\n") )
    {
        close(fd);
        return(-1);
    }
    read_port(fd, msg, sizeof(msg));
    piboxLogger(LOG_TRACE1, "AT response: %s\n", msg);
    memset(msg, 0, 64);

    /* Specify the 10 digit mobile #. */
    sprintf(buf, "AT+CMGS=\"%s\"\r\n", phoneNumber);
    if ( ! write_to_port(fd, buf) )
    {
        close(fd);
        return(-1);
    }
    read_port(fd, msg, sizeof(msg));
    piboxLogger(LOG_TRACE1, "AT response: %s\n", msg);
    memset(msg, 0, 64);

    /* Send the text message */
    if ( ! write_to_port(fd, textMsg) )
    {
        close(fd);
        return(-1);
    }

    /* Reset from SMS. */
    if ( ! write_to_port(fd, "\x1A") )
    {
        close(fd);
        return(-1);
    }

    close(fd);
    return(0);
}

/*
 *========================================================================
 * Name:   call_state
 * Prototype:  int call_state( void )
 *
 * Description:
 * Gets current call state.
 *
 * Returns:
 * 0 no call in progress.
 * 1 call is in progress.
 *========================================================================
 */
int
call_state()
{
    int status;
    pthread_mutex_lock( &callStateMutex );
    status = current_state;
    pthread_mutex_unlock( &callStateMutex );
    return status;
}

/*
 *========================================================================
 * Name:   call_start
 * Prototype:  int call_start( char *phone )
 *
 * Description:
 * Sets up modem and dials number.
 *
 * Returns:
 * 0 on success or <0 on failure.
 *
 * Notes:
 * "fd" is a module static variable set by init_modem().
 *========================================================================
 */
int
call_start(char *phoneNumber)
{
    char    buf[32];
    char    msg[64];

    if ( call_state() == 1 )
    {
        piboxLogger(LOG_INFO, "Call in progress.  Can't start a new call.\n");
        return(-1);
    }

    if ( init_modem() < 0 )
        return(-1);

    /* Get modem's attention. */
    if ( ! write_to_port(fd, "AT\r") )
    {
        close(fd);
        return(-1);
    }
    read_port(fd, msg, sizeof(msg));
    piboxLogger(LOG_TRACE1, "AT response: %s\n", msg);
    memset(msg, 0, 64);

    /* Dial number */
    sprintf(buf, "ATD %s;\r", phoneNumber);
    if ( ! write_to_port(fd, buf) )
    {
        close(fd);
        return(-1);
    }
    read_port(fd, msg, sizeof(msg));
    piboxLogger(LOG_TRACE1, "AT response: %s\n", msg);
    memset(msg, 0, 64);

    /* Note that a call is in progress. */
    pthread_mutex_lock( &callStateMutex );
    current_state = 1;
    pthread_mutex_unlock( &callStateMutex );

    close(fd);
    return(0);
}

/*
 *========================================================================
 * Name:   call_end
 * Prototype:  int call_end( void )
 *
 * Description:
 * Tears down any current call.
 *
 * Returns:
 * 0 on success or <0 on failure.
 *
 * Notes:
 * "fd" is a module static variable set by init_modem().
 *========================================================================
 */
int
call_end()
{
    char    buf[32];
    char    msg[64];

    if ( init_modem() < 0 )
        return(-1);

    /* Get modem's attention. */
    if ( ! write_to_port(fd, "AT\r") )
    {
        close(fd);
        return(-1);
    }
    read_port(fd, msg, sizeof(msg));
    piboxLogger(LOG_TRACE1, "AT response: %s\n", msg);
    memset(msg, 0, 64);

    /* Hang up */
    sprintf(buf, "ATH\r");
    if ( ! write_to_port(fd, buf) )
    {
        close(fd);
        return(-1);
    }
    read_port(fd, msg, sizeof(msg));
    piboxLogger(LOG_TRACE1, "AT response: %s\n", msg);
    memset(msg, 0, 64);

    pthread_mutex_lock( &callStateMutex );
    current_state = 0;
    pthread_mutex_unlock( &callStateMutex );

    close(fd);
    return(0);
}
